package com.example.assignment1;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MobileArrayAdapter extends ArrayAdapter<String> {
	private final Activity context;
	private final List<String> names;
	private final List<String> types;

	static class ViewHolder {
		public TextView text;
		public ImageView image;
	}

	public MobileArrayAdapter(Activity context, List<String> names,
			List<String> types) {
		super(context, R.layout.list_mobile, names);
		this.context = context;
		this.names = names;
		this.types = types;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;
		if (rowView == null) {
			LayoutInflater inflater = context.getLayoutInflater();
			rowView = inflater.inflate(R.layout.list_mobile, null);
			ViewHolder viewHolder = new ViewHolder();
			viewHolder.text = (TextView) rowView.findViewById(R.id.label);
			viewHolder.image = (ImageView) rowView.findViewById(R.id.logo);
			rowView.setTag(viewHolder);
		}

		ViewHolder holder = (ViewHolder) rowView.getTag();
		String s = names.get(position);
		String t = types.get(position);
		holder.text.setText(s);
		if (t.equals("File")) {
			holder.image.setImageResource(R.drawable.ic_file);
		} else if (t.equals("Folder")){
			holder.image.setImageResource(R.drawable.ic_folder);
		} else {
			holder.image.setImageResource(R.drawable.ic_option);
		}

		return rowView;
	}
}