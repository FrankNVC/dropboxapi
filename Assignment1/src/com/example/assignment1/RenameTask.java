package com.example.assignment1;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

public class RenameTask extends AsyncTask<Void, Integer, String> {

	private DropboxAPI<AndroidAuthSession> mDBApi;

	private String path;
	private String name;
	private String parentPath;
	private String result;
	
	private Context mContext;

	public RenameTask(DropboxAPI<AndroidAuthSession> mDBApi, MainActivity main, String path, String newName, String parentPath) {
		this.mDBApi = mDBApi;
		this.path = path;
		this.name = newName;
		this.parentPath = parentPath;
		
		Log.i("Path", path);
		Log.i("new name", name);
		
		mContext = main;
	}
	
	protected String doInBackground(Void... params) {
		try {
			mDBApi.move(path, parentPath+"/"+name);
			result = "Renamed " + name;
		} catch (DropboxException e) {
			// TODO Auto-generated catch block
			Log.i("Exception", e.toString());
		}		
		return result;
	}
	
	@Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null)
            Toast.makeText(mContext,result, Toast.LENGTH_LONG).show();
    }
}