package com.example.assignment1;

import java.util.ArrayList;
import java.util.List;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;

public class LocationChooser extends ListActivity {

	private DropboxAPI<AndroidAuthSession> mDBApi;

	private ArrayList<String> names = new ArrayList<String>();
	private ArrayList<String> types = new ArrayList<String>();
	private ArrayList<String> paths = new ArrayList<String>();
	private String oldPath;
	private String path;
	private String name;
	private String result;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.mDBApi = MainActivity.mDBApi;
		
		Intent intent = getIntent();
		oldPath = intent.getStringExtra("OldPath");
		name = intent.getStringExtra("Name");
		
		path = "/";
		
		View footer = getLayoutInflater().inflate(R.layout.footer, null);
	    ListView listView = getListView();
	    //listView.addHeaderView(header);
	    listView.addFooterView(footer);
	    
	    this.setFinishOnTouchOutside(false);
	    
	    Button move = (Button) findViewById(R.id.btn_move);
	    Button cancel = (Button) findViewById(R.id.btn_cancel);
	    
	    move.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				new MoveTask(mDBApi, LocationChooser.this, oldPath, getParentPath(paths.get(1)), name).execute();
				finish();
			}
	    	
	    });
	    
	    cancel.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				result = "Cancel";
				finish();
			}
	    	
	    });
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// get selected items
		String selectedValue = (String) getListAdapter().getItem(position);
		if (selectedValue.contains("Up")) {
			path = paths.get(0);
			onResume();
		} else {
			if (types.get(position).equals("Folder")) {
				path = paths.get(position);
				onResume();
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		ListDropBox drop = new ListDropBox(mDBApi, this, path);
		drop.execute();
	}

	private class ListDropBox extends AsyncTask<Void, Integer, Void> {

		private DropboxAPI<AndroidAuthSession> mDBApi;

		private LocationChooser main;
		private String path;

		public ListDropBox(DropboxAPI<AndroidAuthSession> mDBApi,
				LocationChooser main, String path) {
			this.mDBApi = mDBApi;
			this.main = main;
			this.path = path;
		}

		protected Void doInBackground(Void... params) {
			Entry existingEntry;
			try {
				names = new ArrayList<String>();
				types = new ArrayList<String>();
				paths = new ArrayList<String>();
				if (!path.equals("/")) {
					paths.add(getParentPath(path));
					names.add("Up to " + paths.get(0));
					types.add("Up");
				} else {
					names.add("Root");
					types.add("Root");
					paths.add("/");
				}
				existingEntry = mDBApi.metadata(path, -1, null, true, null);
				if (existingEntry.size != null) {
					List<Entry> files = existingEntry.contents;
					try {
						for (Entry entry : files) {
							paths.add(entry.path);
							names.add(entry.fileName());
							if (entry.isDir) {
								types.add("Folder");
							} else {
								types.add("File");
							}
						}
						publishProgress();
					} catch (NullPointerException ex) {
						Log.i("Null", ex.toString());
					}
				}

			} catch (DropboxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		protected void onProgressUpdate(Integer... values) {
			if (names != null) {
				setListAdapter(new MobileArrayAdapter(main, names, types));
			}
		}
	}

	public String getParentPath(String path) {
		int index = 0;
		for (int i = 0; i < path.length(); i++) {
			if (path.charAt(i) == '/') {
				index = i;
			}
		}
		String parent;
		if (index != 0) {
			parent = path.substring(0, index);
		} else {
			parent = "/";
		}
		return parent;
	}

	public void logOut() {

	}
	
	@Override
	public void finish() {
		Intent intent = new Intent();
		if (result != null) {
			intent.putExtra("returnkey", result);
		} else {
			intent.putExtra("returnkey", "Successful");
		}
		setResult(RESULT_OK, intent);
		super.finish();
	}
}