package com.example.assignment1;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

public class MoveTask extends AsyncTask<Void, Integer, String> {

	private DropboxAPI<AndroidAuthSession> mDBApi;

	private String oldPath;
	private String newPath;
	private String name;
	private String result;
	
	private Context mContext;

	public MoveTask(DropboxAPI<AndroidAuthSession> mDBApi, LocationChooser main, String oldPath, String newPath, String name) {
		this.mDBApi = mDBApi;
		this.oldPath = oldPath;
		this.newPath = newPath;
		this.name = name;
		mContext = main;
	}
	
	protected String doInBackground(Void... params) {
		try {
			mDBApi.move(oldPath, newPath + "/" + name);
			result = "Moved " + name;
		} catch (DropboxException e) {
			// TODO Auto-generated catch block
			Log.i("Exception", e.toString());
		}		
		return result;
	}
	
	@Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null)
            Toast.makeText(mContext,result, Toast.LENGTH_LONG).show();
    }
}