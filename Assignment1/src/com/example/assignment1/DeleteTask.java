package com.example.assignment1;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

public class DeleteTask extends AsyncTask<Void, Integer, String> {

	private DropboxAPI<AndroidAuthSession> mDBApi;

	private String path;
	private String name;
	private String result;
	
	private Context mContext;

	public DeleteTask(DropboxAPI<AndroidAuthSession> mDBApi, MainActivity main, String path, String name) {
		this.mDBApi = mDBApi;
		this.path = path;
		this.name = name;
		
		mContext = main;
	}
	
	protected String doInBackground(Void... params) {
		try {
			mDBApi.delete(path);
			result = "Deleted " + name;
		} catch (DropboxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return result;
	}
	
	@Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(String result) {
        if (result != null)
            Toast.makeText(mContext,result, Toast.LENGTH_LONG).show();
    }
}