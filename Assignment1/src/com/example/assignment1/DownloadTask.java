package com.example.assignment1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.widget.Toast;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.DropboxFileInfo;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;

public class DownloadTask extends AsyncTask<Void, Integer, String> {

	private DropboxAPI<AndroidAuthSession> mDBApi;

	private String path;
	private String name;
	private String result;
	
	private File file;
	
	private Context mContext;
	private ProgressDialog mDialog;

	public DownloadTask(DropboxAPI<AndroidAuthSession> mDBApi, MainActivity main, String path, String name) {
		this.mDBApi = mDBApi;
		this.path = path;
		this.name = name;
		
		mContext = main;
		mDialog = new ProgressDialog(mContext);
		mDialog.setMessage("Downloading " + name);
		mDialog.setIndeterminate(true);
		mDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		mDialog.setCancelable(false);
		
		File sdCard = Environment.getExternalStorageDirectory();
		file = new File (sdCard.getAbsolutePath() + "/" + name);

	}

	@SuppressWarnings("unused")
	protected String doInBackground(Void... params) {
		
		FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(file);			
			DropboxFileInfo info = mDBApi.getFile(path, null, outputStream, null);
			result = "Downloaded " + name;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			result = e.toString();
		} catch (DropboxException e) {
			// TODO Auto-generated catch block
			result = e.toString();
		}
		return result;
	}

	@Override
    protected void onPreExecute() {
        super.onPreExecute();
        mDialog.show();
    }

	@Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected void onPostExecute(String result) {
        mDialog.dismiss();
        if (result != null)
            Toast.makeText(mContext,result, Toast.LENGTH_LONG).show();
    }
}