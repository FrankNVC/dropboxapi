package com.example.assignment1;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.android.AndroidAuthSession;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.widget.ListView;

public class FileChooser extends ListActivity {

	private File currentDir;
	private File[] listFile;
	private FileArrayAdapter adapter;
	private String path;
	private String result;
	private ArrayList<String> f = new ArrayList<String>();
	private DropboxAPI<AndroidAuthSession> mDBApi;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Intent intent = getIntent();
		path = intent.getStringExtra("UploadPath");
		mDBApi = MainActivity.mDBApi;

		currentDir = Environment.getExternalStorageDirectory();
		fill(currentDir);
	}

	private void fill(File f) {
		File[] dirs = f.listFiles();
		this.setTitle("Current Dir: " + f.getName());
		List<Option> dir = new ArrayList<Option>();
		List<Option> fls = new ArrayList<Option>();
		try {
			for (File ff : dirs) {
				if (ff.isDirectory())
					dir.add(new Option(ff.getName(), "Folder", ff
							.getAbsolutePath()));
				else {
					fls.add(new Option(ff.getName(), "File Size: "
							+ ff.length(), ff.getAbsolutePath()));
				}
			}
		} catch (Exception e) {

		}
		Collections.sort(dir);
		Collections.sort(fls);
		dir.addAll(fls);
		if (!f.getName().equalsIgnoreCase("sdcard"))
			dir.add(0, new Option("..", "Parent Directory", f.getParent()));
		adapter = new FileArrayAdapter(FileChooser.this, R.layout.file_view,
				dir);
		this.setListAdapter(adapter);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		Option o = adapter.getItem(position);
		if (o.getData().equalsIgnoreCase("folder")
				|| o.getData().equalsIgnoreCase("parent directory")) {
			currentDir = new File(o.getPath());
			listFile = currentDir.listFiles();
			for (int i = 0; i < listFile.length; i++) {
				f.add(listFile[i].getAbsolutePath());
			}
			UploadTask up = new UploadTask(FileChooser.this, mDBApi, path,
					listFile);
			up.execute();
			result = up.getErrorMessage();
		} else {
			currentDir = new File(o.getPath());
			listFile = new File[1];
			listFile[0] = currentDir;
			UploadTask up = new UploadTask(FileChooser.this, mDBApi, path,
					listFile);
			up.execute();
			result = up.getErrorMessage();
		}
	}
	
	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void finish() {
		Intent intent = new Intent();
		if (result != null) {
			intent.putExtra("returnkey", result);
		} else {
			intent.putExtra("returnkey", "Successful");
		}
		setResult(RESULT_OK, intent);
		super.finish();
	}
}