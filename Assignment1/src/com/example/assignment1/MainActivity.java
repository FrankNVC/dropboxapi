package com.example.assignment1;

import java.util.ArrayList;
import java.util.List;

import com.example.assignment1.MobileArrayAdapter;
import com.dropbox.client2.DropboxAPI;
import com.dropbox.client2.DropboxAPI.Entry;
import com.dropbox.client2.android.AndroidAuthSession;
import com.dropbox.client2.exception.DropboxException;
import com.dropbox.client2.session.AccessTokenPair;
import com.dropbox.client2.session.AppKeyPair;
import com.dropbox.client2.session.Session.AccessType;
//import com.dropbox.sync.android.DbxPath;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends ListActivity {

	final static private String APP_KEY = "ijxz3k79o7dl5l2";
	final static private String APP_SECRET = "olq1zp4bqn45dbj";
	final static private AccessType ACCESS_TYPE = AccessType.DROPBOX;

	// In the class declaration section:
	static DropboxAPI<AndroidAuthSession> mDBApi;

	private static ArrayList<String> names = new ArrayList<String>();
	private static ArrayList<String> types = new ArrayList<String>();
	private static ArrayList<String> paths = new ArrayList<String>();
	private static String path;
	private static String message;
	
	private LocationChooser lc;

	protected Object mActionMode;
	public int selectedItem = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// And later in some initialization function:
		AppKeyPair appKeys = new AppKeyPair(APP_KEY, APP_SECRET);
		AndroidAuthSession session = new AndroidAuthSession(appKeys,
				ACCESS_TYPE);
		mDBApi = new DropboxAPI<AndroidAuthSession>(session);

		// MyActivity below should be your activity class name
		mDBApi.getSession().startAuthentication(MainActivity.this);

		path = "/";
		this.getListView().setLongClickable(true);
		this.getListView().setOnItemLongClickListener(
				new OnItemLongClickListener() {
					// Called when the user long-clicks on someView
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						if (mActionMode != null) {
							return false;
						}
						selectedItem = position;

						// Start the CAB using the ActionMode.Callback defined
						// above
						mActionMode = MainActivity.this
								.startActionMode(mActionModeCallback);
						view.setSelected(true);
						return true;
					}
				});
	}

	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.context_menu, menu);
			return true;
		}

		// Called each time the action mode is shown. Always called after
		// onCreateActionMode, but
		// may be called multiple times if the mode is invalidated.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false; // Return false if nothing is done
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.menu_download:
				new DownloadTask(mDBApi, MainActivity.this,
						paths.get(selectedItem), names.get(selectedItem))
						.execute();
				mode.finish(); // Action picked, so close the CAB
				return true;
			case R.id.menu_delete:
				new DeleteTask(mDBApi, MainActivity.this,
						paths.get(selectedItem), names.get(selectedItem))
						.execute();
				mode.finish();
				path = paths.get(0);
				onResume();
				return true;
			case R.id.menu_rename:
				AlertDialog.Builder renameDialog = new AlertDialog.Builder(
						MainActivity.this);
				renameDialog.setTitle("Rename");
				final EditText newName = new EditText(MainActivity.this);
				newName.setText(names.get(selectedItem));
				renameDialog.setView(newName);

				renameDialog.setPositiveButton("OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface arg0, int arg1) {
								new RenameTask(mDBApi, MainActivity.this, paths
										.get(selectedItem), newName.getText()
										.toString(), getParentPath(paths.get(selectedItem))).execute();
								onResume();
							}
						});

				renameDialog.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface arg0, int arg1) {
							}
						});

				renameDialog.show();
				mode.finish();
				return true;
			case R.id.menu_move:
				Intent intent = new Intent(MainActivity.this, LocationChooser.class);
				message = paths.get(selectedItem);
				intent.setType("text/plain");
				intent.putExtra("OldPath", message);
				intent.putExtra("Name", names.get(selectedItem));
				startActivityForResult(intent, 0);
				mode.finish();
				return true;
			default:
				return false;
			}
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mActionMode = null;
		}
	};

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// get selected items
		String selectedValue = (String) getListAdapter().getItem(position);
		if (selectedValue.contains("Up")) {
			path = paths.get(0);
			onResume();
		} else {
			if (types.get(position).equals("Folder")) {
				path = paths.get(position);
				onResume();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		switch (item.getItemId()) {
		case R.id.menu_upload:
			Intent intent = new Intent(MainActivity.this, FileChooser.class);
			message = getParentPath(paths.get(1));
			intent.setType("text/plain");
			intent.putExtra("UploadPath", message);
			startActivityForResult(intent, 0);
			return true;
		case R.id.menu_create_folder:
			AlertDialog.Builder createFolderDialog = new AlertDialog.Builder(
					MainActivity.this);
			createFolderDialog.setTitle("New Folder");
			final EditText newFolder = new EditText(MainActivity.this);
			createFolderDialog.setView(newFolder);
			createFolderDialog.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
							new CreateFolderTask(mDBApi, MainActivity.this, getParentPath(paths
									.get(1)), newFolder.getText()
									.toString()).execute();
							onResume();
						}
					});

			createFolderDialog.setNegativeButton("Cancel",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface arg0, int arg1) {
						}
					});

			createFolderDialog.show();
			return true;
		case R.id.action_settings:
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == RESULT_OK && requestCode == 0) {
			if (data.hasExtra("returnkey")) {
				String result = data.getExtras().getString("returnkey");
				if (result != null && result.length() > 0) {
					Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
					onResume();
				}
			}
		}
	}

	@Override
	protected void onResume() {
		super.onResume();

		if (mDBApi.getSession().authenticationSuccessful()) {
			try {
				// Required to complete auth, sets the access token on the
				// session
				mDBApi.getSession().finishAuthentication();

				// StrictMode.ThreadPolicy policy = new
				// StrictMode.ThreadPolicy.Builder().permitAll().build();
				// StrictMode.setThreadPolicy(policy);

				AccessTokenPair tokens = mDBApi.getSession()
						.getAccessTokenPair();

				ListDropBox drop = new ListDropBox(mDBApi, this, path);
				drop.execute();
			} catch (IllegalStateException e) {
				Log.i("DbAuthLog", "Error authenticating", e);
			}
		}
	}

	private class ListDropBox extends AsyncTask<Void, Integer, Void> {

		private DropboxAPI<AndroidAuthSession> mDBApi;

		private MainActivity main;
		private String path;

		public ListDropBox(DropboxAPI<AndroidAuthSession> mDBApi,
				MainActivity main, String path) {
			this.mDBApi = mDBApi;
			this.main = main;
			this.path = path;
		}

		protected Void doInBackground(Void... params) {
			Entry existingEntry;
			try {
				names = new ArrayList<String>();
				types = new ArrayList<String>();
				paths = new ArrayList<String>();
				if (!path.equals("/")) {
					paths.add(getParentPath(path));
					names.add("Up to " + paths.get(0));
					types.add("Up");
				} else {
					names.add("Root");
					types.add("Root");
					paths.add("/");
				}
				existingEntry = mDBApi.metadata(path, -1, null, true, null);
				if (existingEntry.size != null) {
					List<Entry> files = existingEntry.contents;
					try {
						for (Entry entry : files) {
							paths.add(entry.path);
							names.add(entry.fileName());
							if (entry.isDir) {
								types.add("Folder");
							} else {
								types.add("File");
							}
						}
						publishProgress();
					} catch (NullPointerException ex) {
						Log.i("Null", ex.toString());
					}
				}

			} catch (DropboxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}

		protected void onProgressUpdate(Integer... values) {
			if (names != null) {
				setListAdapter(new MobileArrayAdapter(main, names, types));
			}
		}
	}

	public String getParentPath(String path) {
		int index = 0;
		for (int i = 0; i < path.length(); i++) {
			if (path.charAt(i) == '/') {
				index = i;
			}
		}
		String parent;
		if (index != 0) {
			parent = path.substring(0, index);
		} else {
			parent = "/";
		}
		return parent;
	}
	
	public void onBackPressed() {
		if(!names.get(0).equals("Root")){
			path = paths.get(0);
			onResume();
		} else {
			logOut();
		}
	}
	
	public void logOut() {
		
	}
}