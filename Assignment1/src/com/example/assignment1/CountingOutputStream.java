package com.example.assignment1;

import java.io.FileOutputStream;
import java.io.FilterOutputStream;
import java.io.IOException;  
   
public class CountingOutputStream extends FilterOutputStream {  
   
    private long count;  
   
    public CountingOutputStream(FileOutputStream out) { super(out); }  
   
    public long getCount() { return count; }  
   
    public void write(int b) throws IOException {  
        super.write(b);  
        count++;  
    }  
   
    public void write(byte b[]) throws IOException {  
        super.write(b);  
        count += b.length;  
    }  
   
    public void write(byte b[], int off, int len) throws IOException {  
        super.write(b);  
        count += len;  
    }  
}  